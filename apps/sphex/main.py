import sys
from dronekit import connect, Vehicle
import sysadmin as SA
import pixhawk, imet

#dataDir = "/home/jwyngaard/work/UVM_18/temp_data"
dataDir = "/home/uvm/DATA/"
datafile = "data.csv"
logfile = "log.log"

def main():
    """Primary data collection loop for sphex"""
    try:

        # Create Data and Log Files
        ND = SA.mk_ND(dataDir)
        fd = open(ND + datafile, "w")
        fl = open(ND + logfile, "w")
        (devices,all_ports)=SA.serial_devices()
        fl.write("\nSerial Ports discovered:")
        for p in all_ports:
            fl.write("\n"+str(p))
        fd.write(
            "\nCO2 (PPM), Latitude, Longitude, Altitude, Air Speed (m/s), Mode, Fixed Satellites, Available Satellites,voltage,current,level,id")

        fl.write("\nCreated log file")
        sys.stdout.flush()
        

        # Connect to imets
        fl.write("\n\n Trying to connect to imets")
        imets,log=imet.openports(devices)
        for l in log:
            fl.write(l)
        sys.stdout.flush()
        
        # Connect to pixhawk
        fl.write("\n\nTrying to connecting to Pixhawk")
        try:
            vv = connect(devices["Pixhawk"], wait_ready=True, baud=57600)
            fl.write("\nSuccessfully Connected to Pixhawk")
            vv.wait_ready('autopilot_version')
        except Exception as e:
            SA.stop([fl,fd],"\nFailed to connect to pixhawk.")
            SA.stop_imets(imets)
            sys.exit()

        # Connect to CO2s

#       while (1):
        for i in range(3):
            # Collect from pixhawk
            print("PH stats")
            stats = pixhawk.get_aircraft_data(vv)
            
            # Collect from imets
            print("Imet stats")
            imets_data=[]
            for i in imets:
                imets_data.append(i.readline())
            print(imets_data)
            print(stats)
    
            #ppm = CO2.readCO2meter(fr, fw)

            #fd.write("\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
            #ppm, stats[0], stats[1], stats[2], stats[3], stats[4], stats[5], stats[6],stats[7],stats[8],stats[9],stats[10]))
            #print("\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
            #ppm, stats[0], stats[1], stats[2], stats[3], stats[4], stats[5], stats[6],stats[7],stats[8],stats[9],stats[10]))

        SA.stop_imets(imets)
        SA.stop_drone(vv)
        SA.stop_files([fl,fd],"stop")
        sys.exit()


    except KeyboardInterrupt:
        SA.stop_imets(imets)
        SA.stop_drone(vv)
        SA.stop_files([fl,fd],"\nKeyboard Interrupt: System stopped, closing down and saving data")
        sys.exit()


if __name__ == '__main__':
        main()
