import serial, argparse,sys
""" Script for testing imet connection"""
def openport(port):
    try:
        ser = serial.Serial(port, baudrate=57600,timeout=3.0)
        print("I opened the port")
    except serial.SerialException as e:
        print("Error opening serial port:",e)
    return ser


def get_imet_time(imet_device):
	l=imet_device.readline()
        return l.split(',')[5:7]

def main():

    parser = argparse.ArgumentParser(description='Get path to port')
    parser.add_argument('port', type=str, nargs='+', help='Give the path to sensor attached, likely /dev/ttyUSB0 or similar')
    args = parser.parse_args()
    port=str(args.port)[2:-2]
    print("This is my port",port)
    
    ser=openport(port)

	#Check device reads
    for i in range(2): 
        line=ser.readline()
        print(line)

    #Get Time
    i_time=get_imet_time(ser)
    print(i_time)

    ser.close()
    sys.exit(0)


if __name__ == "__main__":
    main()


