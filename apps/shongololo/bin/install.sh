#!/bin/bash
sudo aptitude install python3-dev python3-pip virtualenv
virtualenv --python=python3 .venv
source .venv/bin/activate
pip install -r ../requirements.txt
deactivate
