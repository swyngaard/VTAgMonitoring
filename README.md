Repository to maintain digital components of VTAgMonitoring custom built sensor systems

## Documentation
* Primary use docs are available in ./docs/  To generate and render these or simply rebuild:
    ```
    $ cd docs/
    $ make html
    ```
    And view resulting html in a browser
* Manufacturer sensor manuals and documentation is available in ./manuals/

* Pi login details are:
    - user: uvm
    - password: BCO2&ag
    - Wifi SSID: VTAgPi< number corrosponding with the specific pi in use as follows>
        1. Ground station 1
        2. Ground station 2
        3. Ground station 3
        4. Ground station 4
        5. Ground station 5
        6. Hexacopter known as sphex
    - Wifi Password: VTAgMonitoring

## Wifi
- Edit Wifi config in:
sudo vim /etc/hostapd/hostapd.conf

## Pi Config todos
1. Setup wifi
2. Install:
    - firefox
    - python-serial
    - 
3. Enable I2C and Serial using raspi-config
